const express = require('express');
const helpers = require('../lib/sqlHelpers');

const router = express.Router();

const ISSUES_TABLE = 'issues_table_2';
const POLITICIANS_TABLE = 'congress_members';
const STATES_TABLE = 'states_table';
const BILLS_TABLE = 'bills_table_2';
const BILLS_TO_ISSUES_TABLE = 'bills_to_issues';
const STATEMENTS_TABLE = 'statements_table';

const issuesSQLMid = `\
  SELECT CONCAT('/issues/', ${ISSUES_TABLE}.route) AS route, ${ISSUES_TABLE}.name AS title, CONCAT(${ISSUES_TABLE}.name, ' Description ', summary, ' Credits to iSideWith Essential Question ', \
    question, ' Credits to iSideWith Total Votes ', CONVERT(votes.votes_total, CHAR), ' Votes for ', CONVERT(votes_yes*100/votes.votes_total, CHAR), ' Votes against ', \
    CONVERT(votes_no*100/votes.votes_total, CHAR), ' Votes neutral ', CONVERT((votes.votes_total - votes_yes - votes_no)*100/votes.votes_total, CHAR), ' Category ', category, ' Learn More ', side_yes, ' ', side_no, ' ', side_center, \
    ' Recent Congressional Statements ', \
    CASE WHEN ${ISSUES_TABLE}.statement_1 IS NOT NULL THEN \
      CONCAT(statement_1.title, ' by ', statement_1.name, ' on ', statement_1.date, ' in ', statement_1.state) \
    ELSE '' END, ' ', \
    CASE WHEN ${ISSUES_TABLE}.statement_2 IS NOT NULL THEN \
      CONCAT(statement_2.title, ' by ', statement_2.name, ' on ', statement_2.date, ' in ', statement_2.state) \
    ELSE '' END, ' ', \
    CASE WHEN ${ISSUES_TABLE}.statement_3 IS NOT NULL THEN \
      CONCAT(statement_3.title, ' by ', statement_3.name, ' on ', statement_3.date, ' in ', statement_3.state) \
    ELSE '' END, ' ', \
    CASE WHEN ${ISSUES_TABLE}.statement_4 IS NOT NULL THEN \
      CONCAT(statement_4.title, ' by ', statement_4.name, ' on ', statement_4.date, ' in ', statement_4.state) \
    ELSE '' END, ' ', \
    CASE WHEN ${ISSUES_TABLE}.statement_5 IS NOT NULL THEN \
      CONCAT(statement_5.title, ' by ', statement_5.name, ' on ', statement_5.date, ' in ', statement_5.state) \
    ELSE '' END, ' ', \
    CASE WHEN ${ISSUES_TABLE}.statement_1 IS NOT NULL THEN \
      'Find more info about the representatives that wrote these statements Credits to Congress.gov and ProPublica API' \
    ELSE 'None' END, \
    ' Recent Congressional Bills ', \
    CASE WHEN ${ISSUES_TABLE}.bill_1 IS NOT NULL THEN \
      CONCAT(bill_1.title, ' by ', bill_1.sponsor_name, ' ', \
      CASE WHEN bill_1.sponsor_party = 'R' THEN '(Republican)' ELSE '(Democrat)' END, ' in ', bill_1.sponsor_state) \
    ELSE '' END, ' ', \
    CASE WHEN ${ISSUES_TABLE}.bill_2 IS NOT NULL THEN \
      CONCAT(bill_2.title, ' by ', bill_2.sponsor_name, ' ', \
      CASE WHEN bill_2.sponsor_party = 'R' THEN '(Republican)' ELSE '(Democrat)' END, ' in ', bill_2.sponsor_state) \
    ELSE '' END, ' ', \
    CASE WHEN ${ISSUES_TABLE}.bill_3 IS NOT NULL THEN \
      CONCAT(bill_3.title, ' by ', bill_3.sponsor_name, ' ', \
      CASE WHEN bill_3.sponsor_party = 'R' THEN '(Republican)' ELSE '(Democrat)' END, ' in ', bill_3.sponsor_state) \
    ELSE '' END, ' ', \
    CASE WHEN ${ISSUES_TABLE}.bill_4 IS NOT NULL THEN \
      CONCAT(bill_4.title, ' by ', bill_4.sponsor_name, ' ', \
      CASE WHEN bill_4.sponsor_party = 'R' THEN '(Republican)' ELSE '(Democrat)' END, ' in ', bill_4.sponsor_state) \
    ELSE '' END, ' ', \
    CASE WHEN ${ISSUES_TABLE}.bill_5 IS NOT NULL THEN \
      CONCAT(bill_5.title, ' by ', bill_5.sponsor_name, ' ', \
      CASE WHEN bill_5.sponsor_party = 'R' THEN '(Republican)' ELSE '(Democrat)' END, ' in ', bill_5.sponsor_state) \
    ELSE '' END, ' ', \
    CASE WHEN ${ISSUES_TABLE}.bill_1 IS NOT NULL THEN \
      'Find more info about how different states view these bills Credits to Congress.gov and ProPublica API' \
    ELSE 'None' END) \
    AS search_string \
  FROM ${ISSUES_TABLE} \
    LEFT JOIN (SELECT votes.route, CASE \
      WHEN votes.votes_predict < votes.votes_total THEN votes.votes_total \
      ELSE votes.votes_predict \
      END AS votes_total \
      FROM ( \
        SELECT route, (votes_yes + votes_no) AS votes_predict, votes_total FROM issues_table_2) \
      AS votes) AS votes ON issues_table_2.route=votes.route \
    LEFT JOIN (SELECT * FROM ${STATEMENTS_TABLE}) AS statement_1 ON ${ISSUES_TABLE}.statement_1=statement_1.id \
    LEFT JOIN (SELECT * FROM ${STATEMENTS_TABLE}) AS statement_2 ON ${ISSUES_TABLE}.statement_2=statement_2.id \
    LEFT JOIN (SELECT * FROM ${STATEMENTS_TABLE}) AS statement_3 ON ${ISSUES_TABLE}.statement_3=statement_3.id \
    LEFT JOIN (SELECT * FROM ${STATEMENTS_TABLE}) AS statement_4 ON ${ISSUES_TABLE}.statement_4=statement_4.id \
    LEFT JOIN (SELECT * FROM ${STATEMENTS_TABLE}) AS statement_5 ON ${ISSUES_TABLE}.statement_5=statement_5.id \
    LEFT JOIN (SELECT * FROM ${BILLS_TABLE}) AS bill_1 ON ${ISSUES_TABLE}.bill_1=bill_1.bill_id \
    LEFT JOIN (SELECT * FROM ${BILLS_TABLE}) AS bill_2 ON ${ISSUES_TABLE}.bill_2=bill_2.bill_id \
    LEFT JOIN (SELECT * FROM ${BILLS_TABLE}) AS bill_3 ON ${ISSUES_TABLE}.bill_3=bill_3.bill_id \
    LEFT JOIN (SELECT * FROM ${BILLS_TABLE}) AS bill_4 ON ${ISSUES_TABLE}.bill_4=bill_4.bill_id \
    LEFT JOIN (SELECT * FROM ${BILLS_TABLE}) AS bill_5 ON ${ISSUES_TABLE}.bill_5=bill_5.bill_id`;

const politiciansSQLMid = `\
  SELECT CONCAT('/politicians/', REPLACE(LOWER(full_name), ' ', '-')) AS route, full_name AS title, \
    CONCAT('Title ', short_title, ' First Name ', first_name, ' Last Name ', last_name, ' State ', ${POLITICIANS_TABLE}.state, ' Party ', party, \
    ' ', full_name, ' ', title, ' Phone ', phone, ' Office ', office, ' Birthday ', date_of_birth, ' Tweets by @', twitter_account, \
    ' Embed View on Twitter Related People ', REGEXP_REPLACE(related, CONCAT('(', full_name, ',)|(', full_name, ')'), ''), ' Related bills ', bills) AS search_string \
  FROM ${POLITICIANS_TABLE} \
    LEFT JOIN (SELECT GROUP_CONCAT(full_name SEPARATOR ", ") AS related, state FROM ${POLITICIANS_TABLE} GROUP BY state) AS related_table ON related_table.state=${POLITICIANS_TABLE}.state \
    LEFT JOIN (SELECT GROUP_CONCAT(CONCAT(${BILLS_TABLE}.bill_id, ' ', title, ' Related issues ', related_issues) SEPARATOR ", ") AS bills, sponsor_name FROM ${BILLS_TABLE} \
      LEFT JOIN (SELECT GROUP_CONCAT(issue SEPARATOR ", ") AS related_issues, bill_id FROM ${BILLS_TO_ISSUES_TABLE} GROUP BY bill_id) AS bill_to_issues ON bill_to_issues.bill_id=${BILLS_TABLE}.bill_id \
    GROUP BY sponsor_name) AS bills_table ON bills_table.sponsor_name=${POLITICIANS_TABLE}.full_name`;

const statesSQLMid = `\
  SELECT CONCAT('/states/', REPLACE(LOWER(name), ' ', '-')) AS route, name AS title, \
    CONCAT('Abbrev. ', abbrev, ' Flag State ', name, ' Population ', CONVERT(population, CHAR), ' Legislature ', legislature_name, \
    ' ', nickname, ' Governor ', governor_name, ' Capital ', capital, ' Motto(s) ', motto, ' State Flower ', state_flower, \
    ' Senators and Representatives ', reps, ' State Bills ', bills) AS search_string \
  FROM ${STATES_TABLE}\
    LEFT JOIN (SELECT GROUP_CONCAT(CONCAT(full_name, ' ', title) SEPARATOR ", ") AS reps, state FROM ${POLITICIANS_TABLE} GROUP BY state) AS sen_table ON sen_table.state=${STATES_TABLE}.abbrev \
    LEFT JOIN (SELECT GROUP_CONCAT(CONCAT(${BILLS_TABLE}.bill_id, ' ', title, ' Related issues ', related_issues) SEPARATOR ", ") AS bills, sponsor_state FROM ${BILLS_TABLE} \
      LEFT JOIN (SELECT GROUP_CONCAT(issue SEPARATOR ", ") AS related_issues, bill_id FROM ${BILLS_TO_ISSUES_TABLE} GROUP BY bill_id) AS bill_to_issues ON bill_to_issues.bill_id=${BILLS_TABLE}.bill_id \
    GROUP BY sponsor_state) AS bills_table ON bills_table.sponsor_state=${STATES_TABLE}.abbrev`;

function formatter(SQL, queries) {
  const SQLHead = `${"\
  SELECT SQL_CALC_FOUND_ROWS route, title, CONCAT('...', TRIM("
    + "REGEXP_SUBSTR(\
    REGEXP_REPLACE(REGEXP_REPLACE(search.search_string, '(\\u[0-9]+)|(\"comment\")|(\"perc\")|[^A-Za-z0-9\\' ]', ' '), \"[[:space:]]+\", ' '), \
    '.{0,100}"}${queries[0]}.{0,100}')`
    + '), \'...\') AS search_string FROM (';
  let SQLMid = SQL;
  for (let i = 0; i < queries.length; i += 1) {
    SQLMid = `SELECT * FROM (${SQLMid}) AS search WHERE search.search_string REGEXP ?`;
  }
  const SQLHind = ') AS search';
  return SQLHead + SQLMid + SQLHind;
}

function allSQL(queries) {
  const SQL = `${issuesSQLMid} UNION ${politiciansSQLMid} UNION ${statesSQLMid}`;
  return formatter(SQL, queries);
}

function issuesSQL(queries) {
  return formatter(issuesSQLMid, queries);
}

function politiciansSQL(queries) {
  return formatter(politiciansSQLMid, queries);
}

function statesSQL(queries) {
  return formatter(statesSQLMid, queries);
}

function searchSQLGeneratorFactory(SQLfn) {
  return function searchSQLGenerator(req) {
    const queries = req.query.q.split(' ');
    queries.sort((a, b) => b.length - a.length);
    let sql = SQLfn(queries);
    let page = 0;
    if (req.query.p) {
      page = parseInt(req.query.p, 10);
    }
    sql += ` ORDER BY title LIMIT 10 OFFSET ${page * 10}; SELECT FOUND_ROWS() as elements;`;
    return [sql, queries];
  };
}

router.get('/all', helpers.sqlRequestFactory(searchSQLGeneratorFactory(allSQL)));
router.get('/issues', helpers.sqlRequestFactory(searchSQLGeneratorFactory(issuesSQL)));
router.get('/politicians', helpers.sqlRequestFactory(searchSQLGeneratorFactory(politiciansSQL)));
router.get('/states', helpers.sqlRequestFactory(searchSQLGeneratorFactory(statesSQL)));

module.exports = router;
