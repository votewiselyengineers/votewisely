const chai = require('chai');

const { expect } = chai;
const sinon = require('sinon');
const reps = require('../routes/reps');

describe('Reps', () => {
  it('repsSQLGeneratorFactory should receive a string type argument and return an SQL statement and its arguments', () => {
    const mockReq = sinon.mock();
    mockReq.query = sinon.mock();
    mockReq.query.id = '1234';
    mockReq.query.state = 'CA';
    mockReq.query.last_name = 'Trump';
    const type = 'senate';
    const f = reps.repsSQLGeneratorFactory(type);
    expect(f).to.be.an('function');
    const [testSQL, testSQLargs] = f(mockReq);
    expect(testSQL).to.equal("SELECT * FROM congress_members WHERE id=? AND state=? AND last_name=? AND short_title='Sen.' ");
    expect(testSQLargs).to.have.length(3);
  });
});
