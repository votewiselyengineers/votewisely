function onConnectionClose(err) {
  if (err) {
    return console.log(`error: ${err.message}`);
  }
  console.log('Closed the database connection.');
}

// Requires processFn to be a closure that can take the result of a sql query as an argument.
function jsonResponseFactory(res, processFn) {
  return function jsonResponse(error, results, fields) {
    if (error) {
      res.json({
        status: 500,
        error,
        response: null,
      });
    } else {
      res.json({
        status: 200,
        error: null,
        response: (typeof processFn !== 'undefined') ? processFn(results) : results,
      });
    }
  };
}

// Requires sql_fn to be a closure that can take the result of a backend request object
// from Express as an argument, and return a sql statement with its user arguments.
function sqlRequestFactory(sqlFn, processFn) {
  return function sqlRequest(req, res) {
    const [sql, args] = sqlFn(req);
    res.locals.connection.query(sql, args, jsonResponseFactory(res, processFn));
    res.locals.connection.end(onConnectionClose);
  };
}

module.exports = {
  sqlRequestFactory,
  jsonResponseFactory,
  onConnectionClose,
};
