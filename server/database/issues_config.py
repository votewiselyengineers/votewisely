issues_config = {
    "Immigration": {
        "page": "Immigration_to_the_United_States",
        "public_opinion": (
            "Immigration_to_the_United_States",
            "Public_opinion",
            slice(0, 3),
        ),
    },
    "Abortion": {
        "page": "Abortion_in_the_United_States",
        "public_opinion": (
            "Abortion_in_the_United_States",
            "Public_opinion",
            slice(0, 2),
        ),
    },
    "LGBTQ": {
        "page": "LGBT_rights_in_the_United_States",
        "public_opinion": (
            "LGBT_rights_in_the_United_States",
            "Public_opinion",
            slice(0, 1),
        ),
    },
    "Healthcare": {
        "page": "Health_care_in_the_United_States",
        "public_opinion": (
            "Healthcare_reform_in_the_United_States",
            "Public_opinion",
            slice(1, 2),
        ),
    },
    "Capital Punishment": {
        "page": "Capital_punishment_in_the_United_States",
        "public_opinion": (
            "Capital_punishment_in_the_United_States",
            "Public_opinion",
            slice(0, 1),
        ),
    },
    "Euthanasia": {
        "page": "Euthanasia_in_the_United_States",
        "public_opinion": (
            "Euthanasia_in_the_United_States",
            "U.S._public_opinion_on_euthanasia",
            slice(0, 1),
        ),
    },
    "Gun Politics": {
        "page": "Gun_politics_in_the_United_States",
        "public_opinion": (
            "Gun_politics_in_the_United_States",
            "Public_opinion",
            slice(1, 2),
        ),
    },
    "Minimum Wage": {
        "page": "Minimum_wage_in_the_United_States",
        "public_opinion": ("Minimum_wage_in_the_United_States", "Polls", slice(0, 1)),
    },
    "Climate Change": {
        "page": "Climate_change_in_the_United_States",
        "public_opinion": (
            "Climate_change_in_the_United_States",
            "Political_ideologies",
            slice(1, 4),
        ),
    },
}
