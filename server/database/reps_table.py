import mysql.connector
from mysql.connector import errorcode
import requests

config = {
    "user": "votewisely",
    "password": "votewisely123",
    "host": "cs373votewisely.cqqkkpkbowt8.us-east-1.rds.amazonaws.com",
    "port": "3306",
    "database": "VoteWiselyDB",
    "raise_on_warnings": True,
}

us_state_abbrev = {
    "Alabama": "AL",
    "Alaska": "AK",
    "Arizona": "AZ",
    "Arkansas": "AR",
    "California": "CA",
    "Colorado": "CO",
    "Connecticut": "CT",
    "Delaware": "DE",
    "Florida": "FL",
    "Georgia": "GA",
    "Hawaii": "HI",
    "Idaho": "ID",
    "Illinois": "IL",
    "Indiana": "IN",
    "Iowa": "IA",
    "Kansas": "KS",
    "Kentucky": "KY",
    "Louisiana": "LA",
    "Maine": "ME",
    "Maryland": "MD",
    "Massachusetts": "MA",
    "Michigan": "MI",
    "Minnesota": "MN",
    "Mississippi": "MS",
    "Missouri": "MO",
    "Montana": "MT",
    "Nebraska": "NE",
    "Nevada": "NV",
    "New Hampshire": "NH",
    "New Jersey": "NJ",
    "New Mexico": "NM",
    "New York": "NY",
    "North Carolina": "NC",
    "North Dakota": "ND",
    "Ohio": "OH",
    "Oklahoma": "OK",
    "Oregon": "OR",
    "Pennsylvania": "PA",
    "Rhode Island": "RI",
    "South Carolina": "SC",
    "South Dakota": "SD",
    "Tennessee": "TN",
    "Texas": "TX",
    "Utah": "UT",
    "Vermont": "VT",
    "Virginia": "VA",
    "Washington": "WA",
    "West Virginia": "WV",
    "Wisconsin": "WI",
    "Wyoming": "WY",
}


def get_request():
    """
    Make GET API call to the URL using API_KEY
    :return:
    """
    chamber = "house"
    url = f"https://api.propublica.org/congress/v1/115/{chamber}/members.json"
    API_key = "3ppQzjgDtCX0U5z3aVHvf8LUcnZWaov6Qr87iyI4"

    params = {"X-API-Key": API_key}
    res = requests.get(url=url, headers=params)
    if res.status_code != 200:
        raise Exception(f"GET /tasks/ {res.status_code}")
    else:
        print("API GET request successful!")
    return res.json()


def create_table(table):
    table_description = (
        "  CREATE TABLE IF NOT EXISTS `congress_members` ("
        "   `id` char(10) NOT NULL,"
        "   `title` char(30) NOT NULL,"
        "   `short_title` char(10) NOT NULL,"
        "   `first_name` char(20) NOT NULL,"
        "   `last_name` char(20) NOT NULL,"
        "   `date_of_birth` char(20) NOT NULL,"
        "   `gender` char(10) NOT NULL,"
        "   `party` char(10) NOT NULL,"
        "   `twitter_account` char(64),"
        "   `facebook_account` char(64),"
        "   `youtube_account` char(64),"
        "   `votesmart_id` char(10),"
        "   `url` char(128),"
        "   `contact_form` char(128),"
        "   `in_office` INT,"
        "   `total_votes` INT,"
        "   `missed_votes` INT,"
        "   `office` CHAR(64),"
        "   `phone` CHAR(16),"
        "   `state` CHAR(4) NOT NULL"
        ") ENGINE=InnoDB"
    )
    try:
        print(f"Creating table {table}: ")
        cursor.execute(table_description)
    except mysql.connector.Error as e:
        if e.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(e.msg)
    else:
        print("OK")


def insert_data(members, table):
    attributes = (
        "id",
        "title",
        "short_title",
        "first_name",
        "last_name",
        "date_of_birth",
        "gender",
        "party",
        "twitter_account",
        "facebook_account",
        "youtube_account",
        "votesmart_id",
        "url",
        "contact_form",
        "total_votes",
        "missed_votes",
        "office",
        "phone",
        "state",
    )
    add_member = (
        f"INSERT INTO {table} "
        "(id, title, short_title, first_name, last_name, date_of_birth, gender, party, twitter_account, facebook_account, youtube_account, votesmart_id, url, contact_form, total_votes, missed_votes, office, phone, state) "
        "VALUES (%(id)s, %(title)s, %(short_title)s, %(first_name)s, %(last_name)s, %(date_of_birth)s, %(gender)s, %(party)s, %(twitter_account)s, %(facebook_account)s, %(youtube_account)s, %(votesmart_id)s, %(url)s, %(contact_form)s, %(total_votes)s, %(missed_votes)s, %(office)s, %(phone)s, %(state)s)"
    )
    for member in members:
        member_data = {key: member[key] for key in attributes}
        cursor.execute(add_member, member_data)
    print("Insertion completed!")
    return 0


def update_table_profile_pic(members, table):
    attributes = "id"
    update_member = f"UPDATE {table} " "SET img_url=%(img_url)s " "WHERE id=%(id)s"
    for member in members:
        member_data = {"id": member["id"]}
        member_data[
            "img_url"
        ] = f"https://theunitedstates.io/images/congress/original/{member['id']}.jpg"
        cursor.execute(update_member, member_data)

    print("Update completed!")
    return 0


def update_table_state_full_name(table):
    update_member = (
        f"UPDATE {table} "
        "SET state_fullname=%(state_fullname)s "
        "WHERE state=%(state)s"
    )
    for key, value in us_state_abbrev.items():
        member_data = {"state": value}
        member_data["state_fullname"] = key
        cursor.execute(update_member, member_data)

    print("Update completed!")
    return 0


try:
    cnx = mysql.connector.connect(**config)
    print("Connection successful!")
    cursor = cnx.cursor()
    table_name = "congress_members"

    if __name__ == "__main__":
        # r = get_request()

        # results = r["results"][0]
        # print(f"Num results queried:{results['num_results']}")

        # table_name = "congress_members"
        # # create_table(table_name)
        # # insert_data(results["members"], table_name)
        # update_table_profile_pic(results["members"], table_name)
        update_table_state_full_name(table_name)
        cnx.commit()

except mysql.connector.Error as err:
    if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
        print("Something is wrong with your user name or password")
    elif err.errno == errorcode.ER_BAD_DB_ERROR:
        print("Database does not exist")
    else:
        print(err)
else:
    cursor.close()
    cnx.close()
