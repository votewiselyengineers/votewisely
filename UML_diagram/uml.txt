@startuml
class Issues {
    route           varchar(256)    not null unique PK
    name            varchar(256)    not null
    src             varchar(1024)   not null
    category        varchar(256)    not null
    votes_yes       int default 0   not null
    votes_no        int default 0   not null
    votes_total     int default 0   not null
    side_yes        text            not null
    side_no         text            not null
    side_center     text            not null
    summary         text            not null
    question        varchar(1024)   not null
    icon            varchar(1024)   not null
    img1            varchar(1024)   null
    img2            varchar(1024)   null
    banner          varchar(1024)   null
    bills           text            not null
    statements      text            not null
    pie             varchar(256)    not null
    bill_1          char(20)        null FK
    bill_2          char(20)        null FK
    bill_3          char(20)        null FK
    bill_4          char(20)        null FK
    bill_5          char(20)        null FK
    statement_1     int             null FK
    statement_2     int             null FK
    statement_3     int             null FK
    statement_4     int             null FK
    statement_5     int             null FK
}

class Bills {
    bill_id                     char(20)     not null unique PK
    number                      char(20)     not null
    bill_uri                    char(128)    not null
    title                       text         not null
    sponsor_title               char(20)     not null
    bill_type                   char(20)     not null
    sponsor_id                  char(10)     not null FK
    sponsor_name                char(64)     not null
    sponsor_state               char(10)     not null FK
    sponsor_party               char(10)     not null
    introduced_date             char(20)     not null
    committees                  text         not null
    congressdotgov_url          varchar(256) not null
    primary_subject             char(64)     not null
    summary_short               text         not null
    latest_major_action_date    char(20)     not null
    latest_major_action         text         not null
}

class Congress_Members {
    id                  char(10)    not null unique PK
    title               char(30)    not null
    short_title         char(10)    not null
    first_name          char(20)    not null
    last_name           char(20)    not null
    full_name           char(40)    not null
    date_of_birth       char(20)    not null
    party               char(10)    not null
    twitter_account     char(64)    null
    facebook_account    char(64)    null
    youtube_account     char(64)    null
    votesmart_id        char(10)    null
    url                 char(128)   null
    contact_form        char(128)   null
    total_votes         int         null
    missed_votes        int         null
    office              char(64)    null
    phone               char(16)    null
    state               char(4)     not null FK
    img_url             char(255)   null
    wiki_url            char(255)   null
    gender              char(10)    not null
    state_fullname      char(20)    null
}

class States {
    abbrev              char(10)    not null PK
    name                char(30)    not null
    population          int         not null
    banner_url          char(128)   not null
    legislature_name    char(128)   not null
    motto               char(255)   null
    nickname            char(255)   null
    state_flower        char(128)   null
    capital             char(64)    null
    governor_name       char(40)    null
}

class Statements {
    id              int             auto_increment unique PK
    url             text            not null
    date            char(20)        not null
    title           text            not null
    member_id       char(20)        not null FK
    name            varchar(256)    not null
    chamber         varchar(256)    not null
    state           char(20)        not null FK
    party           char(20)        not null
}

Issues -[hidden]> Bills
Issues -[hidden]-> States
States -[hidden]> Congress_Members

Issues "1" o- "0..*" Bills
Issues "1" o- "0..*" Statements
Congress_Members "1" o- "0..*" Bills
Congress_Members "1" o- "1" States
Bills "1" o- "1" Congress_Members
Bills "1" o- "1" States  
Bills "1" o- "1..*" Issues
Statements "1" o- "1" Congress_Members
Statements "1" o- "1" States
States "1" o- "1..*" Congress_Members
@enduml