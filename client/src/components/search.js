import React, {Component} from 'react';
import {Link} from "react-router-dom";
import '../css/issues.css';

import { nodelink } from '../api';

export default class Search extends Component {
  render() {
    return (
      <div>
        <form>
          <div class="search form-row align-items-center">
            <div class="col-auto">
              <label class="sr-only" for="inlineFormInput">Name</label>
              <input type="text" class="form-control mb-2" id="inlineFormInput" placeholder="Search"></input>
            </div>
            <div class="col-auto">
              <button type="submit" class="btn btn-primary mb-2">Submit</button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}
