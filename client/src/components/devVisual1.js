import React, {PureComponent} from 'react';
import * as d3 from "d3";

export default class DevVisual1 extends PureComponent{


  constructor(props) {
    super(props);
    this.state = {
      width : 800,
      height : 500,
      congress_members: [],
      issue_support_dict: [],
      isLoaded: false
    };
  }

  componentDidMount() {
      fetch("https://api.rainbowconnection.me/v1/congress/all")
        .then(res => res.json())
        .then(res => this.setState({congress_members: res}))
        .then(res => this.setState({isLoaded: true}))
        .then(res => this.sortReps(this.state.congress_members))
        .then(res => this.drawChart());
  }

  sortReps(data){
    var temp = {};
    for(var i = 0; i < data.length; i++){
      var member = data[i];
      var connections = member.connections;

      var issue = connections[3];
      var issue_support_bool = connections[4];

      if(!(issue in temp)){
        temp[issue] = [0,0];
      }
      if(issue_support_bool === "True"){
        temp[issue][0]+=1;
      }
      else{
        temp[issue][1]+=1;
      }
    }
    this.setState({issue_support_dict: temp})
  }

  transformRepData(data){

    var newData = [];
    var i = 0;
    for(var key in data){

      var temp_dict = {};
      temp_dict["Issue"] = key;
      temp_dict["Support"] = data[key][0];
      temp_dict["Oppose"] = data[key][1];
      newData[i] = temp_dict;
      i+=1;
    }
    return newData;
  }

  drawChart() {

      var data = this.transformRepData(this.state.issue_support_dict);
      var groupKey = "Issue";
      var keys = ["Support", "Oppose"];
      var margin = ({top: 10, right: 10, bottom: 20, left: 40});
      var height = 500;
      var width = this.state.width;

      var x0 = d3.scaleBand()
        .domain(data.map(d => d[groupKey]))
        .rangeRound([margin.left, width - margin.right])
        .paddingInner(0.1);

        var  x1 = d3.scaleBand()
        .domain(keys)
        .rangeRound([0, x0.bandwidth()])
        .padding(0.05);

        var  y =
      d3.scaleLinear()
        .domain([0, d3.max(data, d => d3.max(keys, key => d[key]))]).nice()
        .rangeRound([height - margin.bottom, margin.top]);

        var color =
       d3.scaleOrdinal()
         .range(["#003366", "#e50000"]);


    const svg = d3.select("#mysvg1");

      svg.append("g")
       .selectAll("g")
       .data(data)
       .join("g")
       .attr("transform", d => `translate(${x0(d[groupKey])},0)`)
       .selectAll("rect")
       .data(d => keys.map(key => ({key, value: d[key]})))
       .join("rect")
       .attr("x", d => x1(d.key))
       .attr("y", d => y(d.value))
       .attr("width", x1.bandwidth())
       .attr("height", d => y(0) - y(d.value))
       .attr("fill", d => color(d.key));

    svg.append("g")
       .call(xAxis);

      svg.append("g")
        .call(yAxis);

     svg.append("g")
        .call(legend);


     function legend(svg){

         const g = svg
         .attr("transform", `translate(${width},0)`)
         .attr("text-anchor", "end")
         .attr("font-family", "sans-serif")
         .attr("font-size", 15)
         .selectAll("g")
         .data(color.domain().slice().reverse())
         .join("g")
         .attr("transform", (d, i) => `translate(0,${i * 20})`);

    g.append("rect")
         .attr("x", -19)
         .attr("width", 19)
         .attr("height", 19)
         .attr("fill", color);

    g.append("text")
         .attr("x", -24)
         .attr("y", 9.5)
         .attr("dy", "0.35em")
         .text(d => d);
      }

    function xAxis(g){
             g.attr("transform", `translate(0,${height - margin.bottom})`)
            .call(d3.axisBottom(x0).tickSizeOuter(0))
            .call(g => g.select(".domain").remove())
            .attr("font-size", 13)
    }

    function yAxis(g){
      g.attr("transform", `translate(${margin.left},0)`)
        .call(d3.axisLeft(y).ticks(null, "s"))
        .call(g => g.select(".domain").remove())
        .call(g => g.select(".tick:last-of-type text").clone()
          .attr("x", 3)
          .attr("text-anchor", "start")
          .attr("font-weight", "bold")
          .text(data.y))
    }


      return svg.node();
  }

  render(){
    return (
      <div style={{background: "whitesmoke", paddingBottom: '40px', borderRadius: '10px', marginTop: '50px'}}>
                <br></br>
                <h5>Support and Opposition of Issue</h5>
          <svg id="mysvg1" width={this.state.width} height = {this.state.height}></svg>
      </div>
    );
  }
}
