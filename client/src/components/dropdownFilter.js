import React, { PureComponent } from 'react';
import { Dropdown } from "react-bootstrap";

export default class DropdownFilter extends PureComponent {
  constructor(props) {
    /* Props args: list, onClick, defaultIndex (optional) */
    super(props);
    this.state = {
      index: this.props.defaultIndex
    };
  }

  async changeIndex(idx) {
    await this.setState({
      index: idx
    });
    var name = this.props.list[idx];
    this.props.onClick(name);
  }

  render() {
    return (
      <div className="selector">
        {
          this.props.list.map((name, idx) =>
            <Dropdown.Item
                key={idx}
                onClick={() => this.changeIndex(idx)}>
              {name}
            </Dropdown.Item>
          )
        }
      </div>
    );
  }
}
