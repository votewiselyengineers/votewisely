import React from 'react';
import { shallow } from 'enzyme';

import County from '../state-counties';

describe("State Counties", () => {
	it("constructors counties image of provided state", () => {
		const wrapper = shallow(<County state="Texas"/>);
		expect(wrapper.find("#state-county-container").find("img").exists()).toEqual(true);
		expect(wrapper.find("#state-county-container").find("img").props().src)
			.toEqual("https://upload.wikimedia.org/wikipedia/commons/a/a6/Texas_Presidential_Election_Results_2016.svg");
	});
});
