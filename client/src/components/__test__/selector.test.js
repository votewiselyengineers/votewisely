import React from 'react';
import { shallow } from 'enzyme';

import Selector from '../selector';

describe("Selector", () => {
	it("renders with correct initial values", () => {
        const handleClick = jest.fn();

		const wrapper = shallow(
			<Selector
				list={['a', 'b', 'c']}
				onClick={handleClick}
				defaultIndex={1} />
		);

        expect(wrapper.find('button').length).toEqual(3);
        expect(wrapper.find('button').at(0).text()).toEqual('a');
        expect(wrapper.find('button').at(0).prop('className')).toEqual('btn');
        expect(wrapper.find('button').at(1).text()).toEqual('b');
        expect(wrapper.find('button').at(1).prop('className')).toEqual('btn btn-primary');
        expect(wrapper.find('button').at(2).text()).toEqual('c');
        expect(wrapper.find('button').at(2).prop('className')).toEqual('btn');
	});

    it("handles click event correctly", async function() {
        const handleClick = jest.fn();

		const wrapper = shallow(
			<Selector
				list={['a', 'b', 'c']}
				onClick={handleClick}
				defaultIndex={1} />
		);

        await wrapper.find('button').at(0).simulate('click');
        expect(handleClick).toHaveBeenCalledTimes(1);

        expect(wrapper.find('button').length).toEqual(3);
        expect(wrapper.find('button').at(0).text()).toEqual('a');
        expect(wrapper.find('button').at(0).prop('className')).toEqual('btn btn-primary');
        expect(wrapper.find('button').at(1).text()).toEqual('b');
        expect(wrapper.find('button').at(1).prop('className')).toEqual('btn');
        expect(wrapper.find('button').at(2).text()).toEqual('c');
        expect(wrapper.find('button').at(2).prop('className')).toEqual('btn');
    });
});
