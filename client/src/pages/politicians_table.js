import React, {PureComponent} from 'react';
import {Helmet} from 'react-helmet';
import PropTypes from 'prop-types';
import Container from 'react-bootstrap/Container';
import MUIDataTable from "mui-datatables";
import HiliteCell from '../components/hiliteCell'
import {nodelink} from '../api';
import Loading from '../imgs/cat_loading_trans.gif';

import SearchBar from '../components/searchBar';
import SearchResults from '../components/searchResults';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

const columns = [
{
  name: "Image",
  options: {
    filter: false,
    searchable: false,
    customBodyRender: (value, tableMeta, updateValue) => {
      return (
        <img
          src={value}
          style={{width:'65px', height:'80px', marginTop:'10px', marginBottom:'10px'}}
          alt=""
        />
      );
    }
  },
},
  {
    name: "Title",
    options: {
      customBodyRender: (value, tableMeta, updateValue) => {
        return (
          <HiliteCell
            value={value}
            search={tableMeta.tableState.searchText}
          />
        );
      },
    }
  },
  {
    name: "First Name",
    options: {
      filter: false,
      customBodyRender: (value, tableMeta, updateValue) => {
        return (
          <HiliteCell
            value={value}
            search={tableMeta.tableState.searchText}
          />
        );
      },
    }
  },
  {
    name: "Last Name",
    options: {
      filter: false,
      customBodyRender: (value, tableMeta, updateValue) => {
        return (
          <HiliteCell
            value={value}
            search={tableMeta.tableState.searchText}
          />
        );
      },
    }
  },
  {
    name: "State",
    options: {
      customBodyRender: (value, tableMeta, updateValue) => {
        return (
          <HiliteCell
            value={value}
            search={tableMeta.tableState.searchText}
          />
        );
      }
    },
  },
  {
    name: "Party",
    options: {
      customBodyRender: (value, tableMeta, updateValue) => {
        return (
          <HiliteCell
            value={value}
            search={tableMeta.tableState.searchText}
          />
        );
      }
    },
  }
]

export default class Politicians extends PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			senList: [],
			senData: [],
      repList: [],
			repData: [],
      allData: [],
      loaded: false,

      query: '',
			value: 0
		}
	}

  static contextTypes = {
    router: PropTypes.object
  }

  getPartyFull(abbrev){
    switch(abbrev){
      case 'R': return "Republican";
      case 'D': return "Democrat";
      case 'I': return "Independent";
      default: return "N/A";
    }
  }

  getSenData() {
		var senNames = [];
		for (var i = 0; i < 101; i++) {
			var dataSlot = [];
			// senNames[i] = this.state.senList[i].first_name + " " + this.state.senList[i].last_name;
      dataSlot[0] =  this.state.senList[i].img_url;
      dataSlot[1] =  this.state.senList[i].short_title;
      dataSlot[2] =  this.state.senList[i].first_name;
			dataSlot[3] =  this.state.senList[i].last_name;
			dataSlot[4] =  this.state.senList[i].state;
      dataSlot[5] = this.getPartyFull(this.state.senList[i].party);
			senNames[i] = dataSlot;
		}
		this.setState({senData: senNames});
	}

  getRepData() {
    var names = [];
    for (var i = 0; i < 450; i++) {
      var dataSlot = [];
      dataSlot[0] =  this.state.repList[i].img_url;
      dataSlot[1] =  this.state.repList[i].short_title;
      dataSlot[2] =  this.state.repList[i].first_name;
      dataSlot[3] =  this.state.repList[i].last_name;
      dataSlot[4] =  this.state.repList[i].state;
      dataSlot[5] = this.getPartyFull(this.state.repList[i].party);
      names[i] = dataSlot;
    }
    this.setState({repData: names});
  }

	componentDidMount() {
		fetch(nodelink + "/api/reps/senate")
			.then(res => res.json())
			.then(res => {
        this.setState({ senList: res.response });
        this.getSenData();
    });
		fetch(nodelink + "/api/reps/house")
			.then(res => res.json())
			.then(res => {
          this.setState({ repList: res.response });
          this.getRepData();
          this.getAllData();
      });
	}

  getAllData() {
    var repData2 = this.state.repData;
    for (var i = 0; i < this.state.senData.length; i++) {
      repData2.push(this.state.senData[i]);
    }
    this.setState({allData: repData2});
    this.setState({loaded: true});
  }

  renderRedirect(data, meta) {
    var rep_name = String(this.state.allData[meta.dataIndex][2]
                    + '-' + this.state.allData[meta.dataIndex][3])
                    .toLowerCase();
    this.context.router.history.push("/politicians/"+rep_name);

  }

  options = {
    filterType: 'checkbox',
  	rowsPerPageOptions: [10, 15, 20],
  	print: false,
  	download: false,
  	selectableRows: false,
  	onRowClick: (data, meta)=>this.renderRedirect(data, meta)
  }

  optionsMobile = {
    filterType: 'checkbox',
  	rowsPerPageOptions: [10, 15, 20],
  	print: false,
  	download: false,
  	responsive: 'scroll',
  	selectableRows: false,
  	onRowClick: (data, meta)=>this.renderRedirect(data, meta)
  }

  renderTable(){
		if(this.state.loaded){
			return(
				<div>
					<MUIDataTable
						title=<div className="table-dir-title">Politicians</div>
					  data={this.state.allData}
					  columns={columns}
					  options={this.options}
						className="table-desk"
					/>

					<MUIDataTable
						title=<div className="table-dir-title">Politicians</div>
					  data={this.state.allData}
					  columns={columns}
					  options={this.optionsMobile}
						className="table-mobile"
					/>
				</div>
			);
		}
		else
			return(
        <div>
					<img src={Loading} alt="" className="glowGif" />
					<h1 className="glowGif" style={{color: 'white', margin: '0'}}>Loading...</h1>
				</div>
			);
	}

  renderSearch(query){
		if(query !== ""){
			return(
				<div style={{background:'white'}}>
					<div className="table-dir-title"
						style={{textAlign: 'left', padding: '2px 0 0 24px', fontSize: '2.5em'}}>
							Search
					</div>
					<SearchBar
						placeholder="Search Politicians' Pages"
						className='search-bar sb-directory'
						searchFunction={(search) => {
							this.setState({query: search})
						}}
					/>
					<SearchResults type="politicians" query={query}/>
				</div>
			);
		}
		else
		return(
			<div style={{background: 'white', paddingBottom: '50px'}}>
				<div className="table-dir-title"
					style={{textAlign: 'left', padding: '2px 0 0 24px', fontSize: '2.5em'}}>
						Search
				</div>
				<SearchBar
					placeholder="Search Politicians' Pages"
					className='search-bar sb-directory'
					searchFunction={(search) => {
						this.setState({query: search})
					}}
				/>
			</div>
		);
	}

  changeTab = (event, value) => {
    this.setState({value});
  }

	render() {

      return (
        <Container id="poli-table-container">
          <Helmet>
            <title>Politicians - Vote Wisely</title>
            <meta
              name="description"
              content={"Learn about the current Congress senators and representatives in the United States. "
              + "Be politically informed and get excited to vote. "
              + "Find more unbiased information on recent controversial issues, politicians, and states "
              + "on Vote Wisely."}
            />
          </Helmet>
          <img src="https://countercurrents.org/wp-content/uploads/2016/06/US-PRESIDENTIAL-ELECTION-0F-2016-9.jpg" alt="" id="statesdir-bg" />

          <Container id="poli-table">
            <Tabs
  	            value={this.state.value}
  	            indicatorColor="primary"
  	            textColor="primary"
  	            variant="fullWidth"
  	            onChange={this.changeTab}
  							style={{background:'white'}}
  	          >
  	            <Tab label="Directory"/>
  	            <Tab label="Search"/>
  	          </Tabs>
  						{this.state.value === 0 &&
  							<div>
  								{this.renderTable()}
  							</div>}
  		        {this.state.value === 1 &&
  							<div>
  								{this.renderSearch(this.state.query)}
  							</div>}
          </Container>


        </Container>
		);
	}
}
