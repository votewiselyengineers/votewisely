import React, {Component} from 'react';
import {Helmet} from 'react-helmet';
import {Container} from 'react-bootstrap';
import SwipeableViews from 'react-swipeable-views';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import DevVisual1 from '../components/devVisual1';
import DevVisual2 from '../components/devVisual2';
import DevVisual3 from '../components/devVisual3';
import Pie from '../components/pie';
import RepsBubbleChart from '../components/repsBubbleChart';
import OurVisual1 from '../components/ourVisual1';

class visualization extends Component {

	constructor(props) {
		super(props);
		this.state = {
			value: 0
		};
	}

	changeTab = (event, value) => {
    	this.setState({value});
  	}

  	changeIndex = index => {
  	    this.setState({ value: index });
  	};


	render(){
		 return(
			<Container id="searchPage-container">
				<Helmet>
					<title>Visualization - Vote Wisely</title>
					<meta
						name="description"
						content={`Learn about bill and politician statistics. `
						+ "Be politically informed and get excited to vote. "
						+ "Find more unbiased information on recent controversial issues, politicians, and states "
						+ "on Vote Wisely."}
					/>
				</Helmet>
	        	<h2>Data Visualization</h2>
	        	<p style={{ padding : "15px"}}>Data Visualization for RainbowConnection.me and VoteWisely using D3</p>
	       		 {/* Create the Tab Bar and adds the headings */}
		        <Tabs
		            value={this.state.value}
		            indicatorColor="primary"
		            textColor="primary"
		            variant="fullWidth"
		            onChange={this.changeTab}
		          >
		            <Tab label="RainbowConnection 1" />
		            <Tab label="RainbowConnection 2" />
		            <Tab label="RainbowConnection 3" />
		            <Tab label="VoteWisely 1" />
		            <Tab label="VoteWisely 2" />
		            <Tab label="VoteWisely 3" />

		          </Tabs>

	       	 {/* This Section used to make the tabs swipeable */}
		        <SwipeableViews
		          index={this.state.value}
		          onChangeIndex={this.changeIndex}
		        >
		 		  <DevVisual1 />
		 		  <DevVisual2 />
		 		  <DevVisual3 />
		 		  <Pie />
                  <RepsBubbleChart />
		 		  <OurVisual1 />
		        </SwipeableViews>
      		</Container>

		 )
	}
}

export default visualization;
