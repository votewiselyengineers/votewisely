import React from 'react';
import { Pagination } from 'react-bootstrap';
import { shallow } from 'enzyme';

import IssueTile from '../../components/issueTile';
import Issues from '../issues';

describe("Issues", () => {
	it("renders loading initially", () => {
		const wrapper = shallow(<Issues />);
		expect(wrapper.find('h1').text()).toEqual('Loading...');
	});

	it('has correct initial state', () => {
		const wrapper = shallow(<Issues />);
		expect(wrapper.state('active')).toEqual(1);
		expect(wrapper.state('searchInput')).toEqual('');
		expect(wrapper.state('issuesConfig')).toEqual({
			"alphabetFilter": "All",
      "alphabetFilter": "None",
      "alphabetSort": "Ascending",
      "categoryFilter": "None",
      "votesFilter": "None",
		});
        expect(wrapper.state('onSearch')).toEqual(false);
        expect(wrapper.state('searchQuery')).toEqual(null);
        expect(wrapper.instance().scroll).toEqual(false);
	});

    it('has correct sorting of issues then put them on display', () => {
        const wrapper = shallow(<Issues />);
		const testIssues = [{
				icon: "test-icon1",
				name: "test-name1",
				summary: "test-summary1",
				link: "test-link1"
			}, {
				icon: "test-icon3",
				name: "test-name3",
				summary: "test-summary3",
				link: "test-link3"
			}, {
				icon: "test-icon4",
				name: "test-name4",
				summary: "test-summary4",
				link: "test-link4"
			}, {
				icon: "test-icon2",
				name: "test-name2",
				summary: "test-summary2",
				link: "test-link2"
			}];
		wrapper.setState({ issues: testIssues });

        wrapper.instance().handleSort('Ascending');
        var displayedIssues = wrapper.state('issuesOnDisplay');
        expect(displayedIssues.length).toEqual(4);
        expect(displayedIssues[0].name).toEqual('test-name1');
        expect(displayedIssues[1].name).toEqual('test-name2');
        expect(displayedIssues[2].name).toEqual('test-name3');
        expect(displayedIssues[3].name).toEqual('test-name4');

        wrapper.instance().handleSort('Descending');
        displayedIssues = wrapper.state('issuesOnDisplay');
        expect(displayedIssues.length).toEqual(4);
        expect(displayedIssues[0].name).toEqual('test-name4');
        expect(displayedIssues[1].name).toEqual('test-name3');
        expect(displayedIssues[2].name).toEqual('test-name2');
        expect(displayedIssues[3].name).toEqual('test-name1');
    });

    it('has correct filtering of issues then put them on display', () => {
        const wrapper = shallow(<Issues />);
		const testIssues = [{
				icon: "test-icon1",
				name: "A",
				summary: "test-summary1",
				link: "test-link1"
			}, {
				icon: "test-icon3",
				name: "B",
				summary: "test-summary3",
				link: "test-link3"
			}, {
				icon: "test-icon4",
				name: "AB",
				summary: "test-summary4",
				link: "test-link4"
			}, {
				icon: "test-icon2",
				name: "CD",
				summary: "test-summary2",
				link: "test-link2"
			}];
		wrapper.setState({ issues: testIssues });

        wrapper.instance().handleAlphabetFilter('D');
        var displayedIssues = wrapper.state('issuesOnDisplay');
        expect(displayedIssues.length).toEqual(0);

        wrapper.instance().handleAlphabetFilter('A');
        var displayedIssues = wrapper.state('issuesOnDisplay');
        expect(displayedIssues.length).toEqual(2);
        expect(displayedIssues[0].name).toEqual('A');
        expect(displayedIssues[1].name).toEqual('AB');

        wrapper.instance().handleAlphabetFilter('None');
        var displayedIssues = wrapper.state('issuesOnDisplay');
        expect(displayedIssues.length).toEqual(4);
        expect(displayedIssues[0].name).toEqual('A');
        expect(displayedIssues[1].name).toEqual('AB');
        expect(displayedIssues[2].name).toEqual('B');
        expect(displayedIssues[3].name).toEqual('CD');
    });

    it('correctly input issues search on event', () => {
        const wrapper = shallow(<Issues />);
        wrapper.setState({ issuesOnDisplay: [] });

        const mockEvent = jest.mock();
        mockEvent.target = {
            value: 'test search string'
        }
        wrapper.find("input").simulate('change', mockEvent);
        expect(wrapper.state('searchInput')).toEqual('test search string');
    });

/*
    it('has correct searching of issues then put them on display', async () => {
        const wrapper = shallow(<Issues />);

        const mockEvent = jest.mock();
        mockEvent.preventDefault = jest.fn();

        const mockSuccessResponse = {
            response: [{
				icon: "test-icon",
				name: "testAname",
				summary: "test a summary",
				link: "test-link"
            }]
        };
        const mockJsonPromise = Promise.resolve(mockSuccessResponse);
        const mockFetchPromise = Promise.resolve({
            json: () => mockJsonPromise
        });
        const spy = jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise);

        wrapper.setState({
            issuesOnDisplay: [],
            searchInput: '"A" a'
        });
        wrapper.find("form").simulate('submit', mockEvent);
        expect(global.fetch).toHaveBeenCalledTimes(1);
        expect(mockEvent.preventDefault).toHaveBeenCalledTimes(1);
        expect(wrapper.state('searchInput')).toEqual('');

        // Thanks to https://github.com/airbnb/enzyme/issues/1153#issuecomment-392282677
        // Always have to wait until all promises are resolved.
        await new Promise(resolve => setTimeout(resolve, 0));
        spy.mockRestore();

        expect(wrapper.state('highlight')).toEqual({
            keywords: ['a'],
            exact: ['A']
        });
        expect(wrapper.state('issuesOnDisplay')).toEqual([{
            icon: "test-icon",
            name: "testAname",
            summary: "test a summary",
            link: "test-link"
        }]);
    });

    it('has correct reset of search of issues then put them on display', async () => {
        const wrapper = shallow(<Issues />);

        const mockSuccessResponse = {
            response: [{
				icon: "test-icon",
				name: "testAname",
				summary: "test a summary",
				link: "test-link"
            }]
        };
        const mockJsonPromise = Promise.resolve(mockSuccessResponse);
        const mockFetchPromise = Promise.resolve({
            json: () => mockJsonPromise
        });
        const spy = jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise);

        wrapper.setState({
            issuesOnDisplay: [],
            searchInput: '"A" a'
        });
        wrapper.find("#reset-button").simulate('click');
        expect(global.fetch).toHaveBeenCalledTimes(0);

        wrapper.setState({
            highlight: {
                keywords: ["test"],
                exact: ["Fake"]
            }
        })
        wrapper.find("#reset-button").simulate('click');
        expect(global.fetch).toHaveBeenCalledTimes(1);

        await new Promise(resolve => setTimeout(resolve, 0));
        spy.mockRestore();

        expect(wrapper.state('searchInput')).toEqual('');
        expect(wrapper.state('highlight')).toEqual({
            keywords: [],
            exact: []
        });
        expect(wrapper.state('issuesOnDisplay')).toEqual([{
            icon: "test-icon",
            name: "testAname",
            summary: "test a summary",
            link: "test-link"
        }]);
    });

    it('has correct rendering of search terms', () => {
        const wrapper = shallow(<Issues />);
        wrapper.setState({
            issuesOnDisplay: [],
            highlight: {
                keywords: ["unsorted", "test"],
                exact: ["Fake", "Sorted"]
            }
        });

        const searchTerms = wrapper.find('#search-terms').find('.badge');

        expect(searchTerms.length).toEqual(4);
        expect(searchTerms.at(0).text()).toEqual("Fake\xA0\xA0\xD7");
        expect(searchTerms.at(1).text()).toEqual("Sorted\xA0\xA0\xD7");
        expect(searchTerms.at(2).text()).toEqual("test\xA0\xA0\xD7");
        expect(searchTerms.at(3).text()).toEqual("unsorted\xA0\xA0\xD7");
    });
*/

	it('has correct handling of pagination from issuesOnDisplay', () => {
		const wrapper = shallow(<Issues />);
		wrapper.setState({
			issuesPerPage: 4,
			issuesOnDisplay: [{}, {}, {}, {}, {}, {}, {}, {}, {}]
		});
		for (var i = 1; i <= 3; i++) {
			wrapper.setState({ active: i });
			expect(wrapper.find(Pagination.First).prop('disabled')).toEqual(1 === wrapper.state('active'));
			expect(wrapper.find(Pagination.Prev).prop('disabled')).toEqual(1 === wrapper.state('active'));
			expect(wrapper.find(Pagination.Next).prop('disabled')).toEqual(3 === wrapper.state('active'));
			expect(wrapper.find(Pagination.Last).prop('disabled')).toEqual(3 === wrapper.state('active'));

			const items = wrapper.find(Pagination.Item);
			expect(items.length).toEqual(3);
			for (var i = 0; i < items.length; i++) {
				expect(items.at(i).prop('active')).toEqual(i + 1 === wrapper.state('active'));
				/* Refer to source code at:
				 * https://react-bootstrap.netlify.com/components/pagination/#page-item-props
				 * 'children' prop has the label value */
				expect(items.at(i).prop('children')).toEqual(i + 1);
			}
		}
	});

	it('renders issue tiles from issuesOnDisplay correctly', () => {
		const wrapper = shallow(<Issues />);
		const testIssues = [{
				route: "test-link1",
				name: "test-name1",
				icon: "test-icon1",
				summary: "test-summary1",
				question: "test-question1",
				votes_total: 56353,
				category: "test-category1",
				pct_yes: 38.3223,
				pct_no: 62.4903
			}, {
				route: "test-link2",
				name: "test-name2",
				icon: "test-icon2",
				summary: "test-summary2",
				question: "test-question2",
				votes_total: 56353,
				category: "test-category2",
				pct_yes: 38.3223,
				pct_no: 62.4903
			}, {
				route: "test-link3",
				name: "test-name3",
				icon: "test-icon3",
				summary: "test-summary3",
				question: "test-question3",
				votes_total: 56353,
				category: "test-category3",
				pct_yes: 38.3223,
				pct_no: 62.4903
			}, {
				route: "test-link4",
				name: "test-name4",
				icon: "test-icon4",
				summary: "test-summary4",
				question: "test-question4",
				votes_total: 56353,
				category: "test-category4",
				pct_yes: 38.3223,
				pct_no: 62.4903
			}];

		wrapper.setState({
			active: 1,
			issuesPerPage: 2
		});
		wrapper.setState({ issuesOnDisplay: testIssues });
		var issueTiles = wrapper.find(IssueTile)
		expect(issueTiles.length).toEqual(2);
		for (var i = 0; i < issueTiles.length; i++) {
			expect(issueTiles.at(i).prop('data')).toEqual(testIssues[i]);
		}
		wrapper.setState({
			active: 2
		});
		issueTiles = wrapper.find(IssueTile)
		expect(issueTiles.length).toEqual(2);
		for (var i = 0; i < issueTiles.length; i++) {
			expect(issueTiles.at(i).prop('data')).toEqual(testIssues[i+2]);
		}

		wrapper.setState({
			active: 1,
			issuesPerPage: 3
		});
		issueTiles = wrapper.find(IssueTile)
		expect(issueTiles.length).toEqual(3);
		for (var i = 0; i < issueTiles.length; i++) {
			expect(issueTiles.at(i).prop('data')).toEqual(testIssues[i]);
		}
		wrapper.setState({
			active: 2
		});
		issueTiles = wrapper.find(IssueTile)
		expect(issueTiles.length).toEqual(1);
		for (var i = 0; i < issueTiles.length; i++) {
			expect(issueTiles.at(i).prop('data')).toEqual(testIssues[i+3]);
		}
	});
});
